let mix = require("laravel-mix");
mix.js("web/static/js/app.js", "web/js/")
    .sass("web/static/css/app.scss", "web/css/");